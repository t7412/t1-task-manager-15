package ru.t1.chubarov.tm.controller;

import ru.t1.chubarov.tm.api.controller.ICommandController;
import ru.t1.chubarov.tm.api.service.ICommandService;
import ru.t1.chubarov.tm.model.Command;
import ru.t1.chubarov.tm.util.FormatUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory    : " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory : " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory   : " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Usage memory   : " + FormatUtil.formatBytes(usageMemory));
    }

    @Override
    public void showArgumentError() {
        System.out.println("[ERROR]");
        System.out.println("This argument not supported");
        System.exit(1);
    }

    @Override
    public void showCommandError() {
        System.out.println("[ERROR]");
        System.out.println("This command not supported");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeni Chubarov");
        System.out.println("e-mail: echubarov@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    @Override
    public void showWelcome() {
        System.out.println("** WELCOME TASK MANAGER **");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

}
