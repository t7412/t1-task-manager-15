package ru.t1.chubarov.tm.api.repository;

import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.TaskNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {
    void add (Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id) throws AbstractException;

    Task findOneByIndex(Integer index) throws AbstractException;

    void remove(Task task);

    Task removeById(String id) throws IdEmptyException;

    Task removeByIndex(Integer index) throws IndexIncorrectException;

    Integer getSize() throws TaskNotFoundException;

}
