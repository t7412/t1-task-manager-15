package ru.t1.chubarov.tm.api.service;

import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.enumerated.Sort;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.entity.TaskNotFoundException;
import ru.t1.chubarov.tm.exception.field.IdEmptyException;
import ru.t1.chubarov.tm.exception.field.IndexIncorrectException;
import ru.t1.chubarov.tm.exception.field.NameEmptyException;
import ru.t1.chubarov.tm.exception.field.TaskIdEmptyException;
import ru.t1.chubarov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService  extends ITaskRepository {

    Task create(String name, String description);

    List<Task> findAll(Sort sort);

    List<Task> findAll(Comparator comparator);

    void remove(Task task);

    Task findOneById(String id) throws AbstractException;

    Task findOneByIndex(Integer index) throws AbstractException;

    Task updateById(String id, String name, String description) throws AbstractException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

    Task removeById(String id) throws IdEmptyException;

    Task removeByIndex(Integer index) throws IndexIncorrectException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractException;

    Task changeTaskStatusById(String id, Status status) throws AbstractException;

}
